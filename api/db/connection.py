import os
import psycopg

def query(func):
    def wrapper(*args, **kwargs):
        with psycopg.connect() as conn:
            with conn.cursor() as cur:
                return cur.execute(*func(*args, **kwargs)).fetchall()
    return wrapper

def extract(func):
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)[0][0]
    return wrapper