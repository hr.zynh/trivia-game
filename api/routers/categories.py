from fastapi import APIRouter
from pydantic import BaseModel

from db import queries

router = APIRouter()

def prefix(str):
    return f"/api/{str}"


@router.get(prefix("categories"))
def categories(page: int = 0):
    return [result_arr[0] for result_arr in queries.get_categories(page)]


## Simple model for body inputs
class Category(BaseModel):
    title: str


@router.post(prefix("categories"))
def create_category(cat: Category): 
    return queries.add_category(cat.title)


@router.put(prefix("categories/{category_id}"))
def put_category(category_id: int, cat: Category):
    return queries.put_category(category_id, cat)


@router.delete(prefix("categories/{category_id}"))
def delete_category(category_id: int):
    return queries.delete_category(category_id)