from db.connection import query, extract


@query
def get_categories(page: int = 0):
    return """
        SELECT row_to_json(categories) FROM categories
        LIMIT 100
        OFFSET %s 
    """, (page * 100,)


@extract
@query
def add_category(name: str):
    return """
        INSERT INTO categories (title, canon)
        VALUES (%s, %s)
        RETURNING row_to_json(categories)
    """, (name, False)


@extract
@query
def put_category(category_id: int, cat):
    return """
        UPDATE categories
        SET title = %s
        WHERE id = %s
        RETURNING row_to_json(categories)
    """, (cat.title, category_id)


@extract
@query
def delete_category(category_id: int):
    return """
        DELETE FROM categories
        WHERE id = %s
        RETURNING row_to_json(categories)
    """, (category_id,)